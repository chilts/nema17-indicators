#!/bin/bash

for FILE in src/*.scad; do
    FILE=$(basename $FILE .scad)
    echo "* src/$FILE.scad -> stl/$FILE.stl"

    openscad \
        --hardwarnings \
        -o stl/${FILE}.stl \
        src/${FILE}.scad

    openscad \
        --autocenter \
        --render 1 \
        --hardwarnings \
        --imgsize 640,480 \
        --colorscheme Starnight \
        -o img/${FILE}.png \
        src/${FILE}.scad
done

montage img/*.png -tile 4x3 -geometry +0+0 -border 10 -bordercolor purple all.png
