# Nema 17 Indicators #

Some extruder/axis indicators:

<a href="https://postimg.cc/rdw6K39t" target="_blank"><img src="https://i.postimg.cc/rdw6K39t/holey.png" alt="holey"/></a><br/><br/>
<a href="https://postimg.cc/hQx6kBKN" target="_blank"><img src="https://i.postimg.cc/hQx6kBKN/nucleus.png" alt="nucleus"/></a><br/><br/>
<a href="https://postimg.cc/jwDGhfmx" target="_blank"><img src="https://i.postimg.cc/jwDGhfmx/oval.png" alt="oval"/></a><br/><br/>
<a href="https://postimg.cc/yJQM9jfp" target="_blank"><img src="https://i.postimg.cc/yJQM9jfp/star.png" alt="star"/></a><br/><br/>
<a href="https://postimg.cc/G866g2nk" target="_blank"><img src="https://i.postimg.cc/G866g2nk/window.png" alt="window"/></a><br/><br/>

## Author ##

Andrew Chilton:

* https://chilts.org
* https://codeberg.org/chilts

## License ##

[Public Domain](https://creativecommons.org/publicdomain/zero/1.0/)
