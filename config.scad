$fn = 72;

indicatorThickness = 5;
indicatorDiameter = 40;
indicatorRadius = indicatorDiameter / 2;

magnetDiameter = 8;
magnetThickness = indicatorThickness + 2;

module magnet() {
  linear_extrude(height = magnetThickness) {
    circle(d = magnetDiameter);
  }
}
