include <../config.scad>

$fn = 72;

epsilon = 0.1;

module inset() {
  translate([0, indicatorRadius - 2, 0])
    circle(r = 14);
}

module Window() {
  linear_extrude(height = indicatorThickness) {
    difference() {
      union() {
        // outside perimiter
        difference() {
          circle(indicatorRadius);
          circle(indicatorRadius - 4);
        }

        difference() {
          // central disk
          circle(indicatorRadius - 4);

          // minus the windows
          for(angle = [0:120:360])
            rotate([0, 0, angle])
              inset();
        }

      }

      // take off the holes
      for(angle = [0:120:360])
        rotate([0, 0, angle + 60])
          translate([0, indicatorRadius - 3.5, 0])
            circle(r = 2);
    }
  }

  magnet();
}

Window();
