include <../config.scad>

$fn = 288;

epsilon = 0.1;

module groovey() {
  linear_extrude(height = indicatorThickness) {
    difference() {
      circle(indicatorRadius);

      for (angle = [0 : 45 : 360]) {
        // petals
        rotate([0, 0, angle])
          hull() {
            translate([0, indicatorRadius - 6])
              circle(r = 4);
            translate([0, 6])
              circle(r = 1);
          }

        // holes
        rotate([0, 0, angle + 45 / 2])
          translate([0, indicatorRadius - 3])
            circle(r = 2);

        // serated edge
        rotate([0, 0, angle + 45 / 2 - 12])
          translate([0, indicatorRadius])
            circle(r = 1.5);
        rotate([0, 0, angle + 45 / 2 + 12])
          translate([0, indicatorRadius])
            circle(r = 1.5);
      }
    }

  }

  magnet();
}

groovey();
