include <../config.scad>

$fn = 288;

epsilon = 0.1;

module target() {
  linear_extrude(height = indicatorThickness) {
    difference() {
      circle(indicatorRadius);
      circle(indicatorRadius - 2);
    }

    difference() {
      circle(indicatorRadius - 4);
      circle(indicatorRadius - 6);
    }

    difference() {
      circle(indicatorRadius - 8);
      circle(indicatorRadius - 10);
    }

    difference() {
      circle(indicatorRadius - 12);
      circle(indicatorRadius - 14);
    }

    // add the spokes
    for(angle = [0:120:360]) {
      rotate([0, 0, angle + 60])
        translate([0, 4])
          square([2, 4], center = true);
      rotate([0, 0, angle])
        translate([0, 8])
          square([2, 4], center = true);
      rotate([0, 0, angle + 60])
        translate([0, 12])
          square([2, 4], center = true);
      rotate([0, 0, angle])
        translate([0, 16])
          square([2, 4], center = true);
    }
  }

  magnet();
}

target();
