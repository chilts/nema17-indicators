include <../config.scad>

$fn = 288;

epsilon = 0.1;

module roundy() {
  linear_extrude(height = indicatorThickness) {
    for (angle = [0:15:360]) {
      rotate([0, 0, angle])
        translate([0, indicatorRadius - 1])
          circle(r = 3);
    }

    for (angle = [0:60:360]) {
      rotate([0, 0, angle + 5])
        translate([0, indicatorRadius - 4.5])
          circle(r = 2.7);
    }
    for (angle = [0:60:360]) {
      rotate([0, 0, angle + 10])
        translate([0, indicatorRadius - 7])
          circle(r = 2.4);
    }
    for (angle = [0:60:360]) {
      rotate([0, 0, angle + 15])
        translate([0, indicatorRadius - 9.5])
          circle(r = 2.1);
    }
    for (angle = [0:60:360]) {
      rotate([0, 0, angle + 20])
        translate([0, indicatorRadius - 11.5])
          circle(r = 1.8);
    }
    for (angle = [0:60:360]) {
      rotate([0, 0, angle + 25])
        translate([0, indicatorRadius - 13.5])
          circle(r = 1.5);
    }
    for (angle = [0:60:360]) {
      rotate([0, 0, angle + 30])
        translate([0, indicatorRadius - 15])
          circle(r = 1.2);
    }
    for (angle = [0:60:360]) {
      rotate([0, 0, angle + 35])
        translate([0, indicatorRadius - 16])
          circle(r = 0.9);
    }
  }

  magnet();
}

roundy();
