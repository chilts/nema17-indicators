include <../config.scad>

$fn = 72;

epsilon = 0.1;

module inset() {
  intersection() {
    translate([14, 6, 0])
      circle(r = 20);
    translate([-12, 14, 0])
      circle(r = 13);
    // translate([-7, -7, 0])
    //   circle(r = 10);
  }
}

module oval() {
  linear_extrude(height = indicatorThickness) {
    circle(7);
    difference() {
      circle(indicatorRadius);
      circle(indicatorRadius - 2);
    }
    for(angle = [0:75:360])
      rotate([0, 0, angle])
        inset();
  }

  magnet();
}

oval();
