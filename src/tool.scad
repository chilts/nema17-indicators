include <../config.scad>

$fn = 288;

epsilon = 0.1;

module tool() {
  linear_extrude(height = indicatorThickness) {
    union() {
      // permieter
      difference() {
        circle(indicatorRadius);
        circle(indicatorRadius - 5);
        for (angle = [0:30:360]) {
          rotate([0, 0, angle])
            translate([0, indicatorRadius, 0])
              circle(3);
        }
      }
    }

    // add the spokes
    for (angle = [0:120:360]) {
      rotate([0, 0, angle]) {
        translate([0, 3])
          square([indicatorDiameter - 6, 2], center = true);
        translate([0, -3])
          square([indicatorDiameter - 6, 2], center = true);
      }
    }
  }

  magnet();
}

tool();
