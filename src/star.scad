include <../config.scad>

$fn = 72;

epsilon = 0.1;

module cutout() {
  polygon(points = [
    [0, indicatorRadius - 1],
    [1, indicatorRadius],
    [-1, indicatorRadius]
  ]);
}

module fluid() {
  linear_extrude(height = indicatorThickness) {
    difference() {
      union() {
        circle(indicatorRadius, $fn = 72);
      }
      circle(indicatorRadius - 2, $fn = 72);

      // remove teeth
      for(angle = [0:15:360]) {
        rotate([0, 0, angle])
          cutout();
      }
    }

    // create a random shape
    for(angle = [0:45:360]) {
      rotate([0, 0, angle])
        hull() {
          translate([-18, 0, 0])
            circle(1, $fn = 16);
          translate([-3, 2, 0])
            circle(3, $fn = 16);
        }
    }

    // for(angle = [0:60:360]) {
    //   rotate([0, 0, angle])
    //     arm();
    // }
  }

  magnet();
}

fluid();
