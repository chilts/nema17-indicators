include <../config.scad>

$fn = 72;

epsilon = 0.1;

module gear() {
  polygon(points = [
    [-1  , indicatorRadius],
    [ 1  , indicatorRadius],
    [ 1.3, indicatorRadius - 3 - epsilon],
    [-1.3, indicatorRadius - 3 - epsilon]
  ]);
}

module arm() {
  square([2, indicatorDiameter - 6], center = true);
}

module nucleus() {
  linear_extrude(height = indicatorThickness) {
    difference() {
      union() {
        circle(indicatorRadius - 3);
        for(angle = [0:15:360]) {
          rotate([0, 0, angle])
            gear();
        }
      }
      circle(indicatorRadius - 6);
    }
    for(angle = [0:60:360]) {
      rotate([0, 0, angle])
        arm();
    }
  }

  magnet();
}

nucleus();
