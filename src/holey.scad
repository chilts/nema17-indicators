// with holes around the edge

include <../config.scad>

$fn = 72;

epsilon = 0.1;

module spreadThreeTimes(trans) {
  for(angle = [0:120:360]) {
    rotate([0, 0, angle]) {
      translate(trans) {
        children();
      }
    }
  }
}

module holey() {
  linear_extrude(height = indicatorThickness) {
    union() {
      difference() {
        circle(r = indicatorRadius - 3, $fn = 36);

        // take off big inner circle
        circle(r = indicatorRadius - 7, $fn = 36);

        // take off the three hole inserts
        spreadThreeTimes([indicatorRadius - 6, 0, 0])
          circle(r = 5, $fn = 36);
      }

      // three holes around the edge
      spreadThreeTimes([indicatorRadius - 6, 0, 0])
        difference() {
          circle(r = 8, $fn = 36);
          circle(r = 5, $fn = 36);
        }

      // three holes around the edge
      spreadThreeTimes([6, 0, 0])
        square([5, 3], center = true);
    }

  }

  magnet();
}

holey();
