include <../config.scad>

$fn = 288;

epsilon = 0.1;

module cutout() {
  linear_extrude(height = indicatorThickness) {
    union() {
      difference() {
        circle(indicatorRadius);
        circle(indicatorRadius - 5);
        for (angle = [0:15:360]) {
          rotate([0, 0, angle])
            translate([0, indicatorRadius, 0])
              circle(3);
        }
      }
    }

    // add the spokes
    for(angle = [0:120:360]) {
      rotate([0, 0, angle])
        translate([0, indicatorRadius / 2])
          difference() {
            circle(r = 7);
            circle(r = 5);
          }
    }
  }

  magnet();
}

cutout();
