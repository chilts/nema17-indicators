include <../config.scad>

$fn = 288;

epsilon = 0.1;

module aSwoosh() {
  intersection() {
    circle(indicatorRadius);
    translate([0, -45, 0]) {
      difference() {
        circle(50);
        circle(47);
      }
    }
  }
}

module swoosh() {
  linear_extrude(height = indicatorThickness) {
    difference() {
      circle(indicatorRadius);
      circle(indicatorRadius - 3);
    }
    for (angle = [0:120:360]) {
      rotate([0, 0, angle])
        aSwoosh();
    }
  }

  magnet();
}

swoosh();
